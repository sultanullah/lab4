import java.util.ArrayList;

import org.junit.Test;


public class MyStackTest {

	@Test
	public void testException0() {
		MyStack stack = new MyStack();
		ArrayList<HtmlTag> test = new ArrayList<HtmlTag>();
		stack.push(test.get(0));
	}
	
	@Test
	public void testException1() {
		MyStack stack = new MyStack();
		stack.peek();
	}
	
	@Test
	public void testException2() {
		MyStack stack = new MyStack();
		stack.pop();
	}
	
	@Test
	public void ThisShouldPass() {
		MyStack stack = new MyStack();
		HtmlTag tag = new HtmlTag("tag");
		HtmlTag tag2 = new HtmlTag("tag2");
		stack.push(tag);
		stack.push(tag2);
		stack.peek();
		stack.pop();
	}
	
	
}
