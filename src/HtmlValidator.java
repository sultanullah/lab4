//This class is used to validate tags from a given html file.

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/**
 * The Class HtmlValidator analyzes a html file and can add, remove, and
 * checks the tags to output an error message if there are any errors within 
 * the html file. This class will examine any html file or url inputted.
 */
public class HtmlValidator {
	//A queue is used to analyze each tag within the html file. A linked list
	//implementation of the queue is used to do operations on the tags.
	private	Queue<HtmlTag> validator = new LinkedList<HtmlTag>();
		
	/**
	 * Constructs a new HtmlValidator object. Creates an empty list of tags.
	 */
	public HtmlValidator(){    
		validator = new LinkedList<HtmlTag>();
	}
	
	/**
	 * Contstructs a new HtmlValidator object from a given Queue of tags.
	 *
	 * @param tags the queue of tags to copied onto a new HtmlValidator object.
	 * @throws IllegalArgumentException if the queue given to be copied is
	 * 			empty.
	 */
	public HtmlValidator(Queue<HtmlTag> tags) throws IllegalArgumentException{
		if(tags == null){
			throw new IllegalArgumentException();
		}
		validator = new LinkedList<HtmlTag>();
		Iterator<HtmlTag> copy = tags.iterator();
		while(copy.hasNext()){
			validator.add(copy.next());
		}
	}
	
	/**
	 * Adds the new tag into the Queue of tags. Modifies the queue to 
	 * include the tag that should be added.
	 *
	 * @param tag the tag to be added onto the queue of html tags. 
	 * @throws IllegalArgumentException if the tag is null.
	 */
	public void addTag(HtmlTag tag) throws IllegalArgumentException{
		if(tag == null){
			throw new IllegalArgumentException();
		}
		validator.add(tag);
	}
	
	/**
	 * Gets all the tags inside the queue.
	 *
	 * @return the tags inside the queue
	 */
	public Queue<HtmlTag> getTags(){
		return validator;
	}
	
	/**
	 * Removes all tags inside the queue which are the element. This includes opening
	 * and closing tags of the argument element.
	 *
	 * @param element the element that is going to be removed, it is a character inside the tag
	 * 		  for example: <p> and </p> contains the element p.
	 * @throws IllegalArgumentException if the element is null.
	 */
	public void removeAll(String element)throws IllegalArgumentException{
		if(element == null){
			throw new IllegalArgumentException();
		}
		int size = validator.size();
		for(int i = 0; i < size; i++){
			if (validator.peek().toString().contains(element)) {
				validator.remove();
			} else {
				validator.add(validator.remove());
			}
		}
	}
		
	/**
	 * The validate method will analyze each tag within the queue of html tags and 
	 * display them. The method checks that all opening tags have a closing tag and that 
	 * there are no tags that should not be in the html file or are unexpected tags. The method
	 * will print the errors and location of errors when it encounters them.
	 *   
	 */
	public void validate(){
		MyStack stack = new MyStack();
		StringBuilder space = new StringBuilder();
		Iterator<HtmlTag> validate = validator.iterator();
		while (validate.hasNext()) {
			HtmlTag current = validate.next();
			if (current.isOpenTag()) {
				if (!current.isSelfClosing()) {
					stack.push(current);
					System.out.println(space.toString() + current);
					space.append("    ");
				} else {
					System.out.println(space.toString() + current);
				}
			} else {				
					if(stack.isEmpty()){
						System.out.println(current);
					} else {
						if(stack.peek().matches(current)){
							stack.pop();
							space.delete(0, 4);
							System.out.println(space.toString() + current);
						} else {
							System.out.println("ERROR unexpected tag:" + current);
						}
					}
				}
			}
		
		while(!stack.isEmpty()){
			System.out.println("ERROR unclosed tag:" + stack.pop());
		}
	}
}