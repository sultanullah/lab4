/*
 * Implementation of a simple stack for HtmlTags.
 * You should implement this class.
 */

import java.util.ArrayList;
/**
 * @author Sultan Ullah(29128113) and Kevin Chan(39975131)This class provides
 *         a representation of a stack through the use of an ArrayList object.
 *         The ArrayList or "stack" holds objects of type HtmlTag. MyStack() 
 *         implements the behavior of the Stack class given in java.util.stack.
 */
public class MyStack {
	// An ArrayList to hold HtmlTag objects.
	private ArrayList<HtmlTag> stack_internal;
	
	/**
	 * Constructs a new MyStack object.
	 */
	public MyStack( ) {
		this.stack_internal = new ArrayList<HtmlTag>( );
	}
	
	/**
	 * Pushes a new object on top of the stack.
	 *
	 * @param tag the HtmlTag object that is going to be 
	 * 		  pushed on top of the stack.
	 */
	public void push( HtmlTag tag ) {
		//Object is added to the ArrayList.
		stack_internal.add(tag);
	}
	
	/**
	 * Pops the object at the top of stack and returns it.
	 *
	 * @return the HtmlTag object at the top of the stack.
	 * 
	 * @throws IndexOutOfBoundsException
	 * 			if the stack is empty.	
	 */
	public HtmlTag pop(){
		if(stack_internal.size() == 0){
			throw new IndexOutOfBoundsException();
		}
		return stack_internal.remove(stack_internal.size() - 1);
	}
	
	/**
	 * Peek looks at the object at the top of the stack and returns it
	 * without removing it.
	 *
	 * @return the HtmlTag object at the top of the stack without.
	 * 
	 * @throws IndexOutofBoundsException
	 * 			if stack is empty. 
	 */
	public HtmlTag peek( ){
		if(stack_internal.size() == 0){
			throw new IndexOutOfBoundsException();
		}
		return stack_internal.get(stack_internal.size() - 1);
	}
	
	/**
	 * Checks if is stack is empty.
	 *
	 * @return true if the stack is empty.
	 */
	public boolean isEmpty( ) {
		return(stack_internal.isEmpty());
	}
}